(function () {
    tinymce.create('tinymce.plugins.CustomHTML', {
        init: function (ed, url) {
          
            ed.addButton('gallery', {
                title: 'Simple Image Gallery',
                image: url + '/img/gallery.png',
                onclick: function () {
                  	
                  	var FullArticleImage = document.getElementById('jform_images_image_fulltext').value;
                  	if (FullArticleImage == null || FullArticleImage == "") {
                      	alert('Please select Full Article Image from Images and Links tab!');
                    } else {
                      	ImageFolder = '{gallery}' + FullArticleImage.substring(0, FullArticleImage.lastIndexOf('/')).replace('images/', '') + '{/gallery}'
                        ed.execCommand('mceToggleFormat', false, 'div');
                      	ed.execCommand('mceInsertContent', false, ImageFolder);
                    }
                  	
                }
            });
          
            ed.addButton('mohe-media-statement', {
                title: 'Template for MoHE Media Statement ',
                image: url + '/img/mohe-media-statement.png',
                onclick: function () {
                    
                    if (confirm('All content in this editor will be replaced with MoHE Media Statement template, Article Category will be set to Kenyataan Media and Show Title in Options tab will be set to Hide! Are you sure?'))  {
                      	
                      	var html = '<div style="text-align: center;"><img src="images/logo_kpt_black.svg" alt="logo kpt black" width="300" height="115" /></div>';
                  		html += '<h1 style="text-align: center;">KENYATAAN MEDIA</h1>';
                  		html += '<h1 style="text-align: center;">[SILA MASUKKAN TAJUK KENYATAAN MEDIA]</h1>';
                  		html += '<hr />';
                  		html += '<p style="text-align: justify;">[SILA MASUKKAN KANDUNGAN]</p>';
                  		html += '<p><strong>Kementerian Pengajian Tinggi<br />[MASUKKAN TARIKH DI SINI]</strong></p>';
                  		html += '<h2>Muat Turun:</h2>'
                    	html += '<p>[FILE MUAT TURUN DI SINI]</p>'
                      	
                  		// Change Options : Show Title to hide
                      	document.getElementById('jform_attribs_show_title').value = '0';
                      	// Change Article Category to Kenyataan Media
                      	document.getElementById('jform_catid').value = '13';
                      	//Update jQuery Choosen v1.0 selected
                  		jQuery('#jform_attribs_show_title, #jform_catid').trigger("liszt:updated");
                      	
                    	ed.execCommand('mceSetContent', false, html);
                    }
                    
                }
            });
          	
          	ed.addButton('vip-media-statement', {
                title: 'Template for VIP Media Statement ',
                image: url + '/img/vip-media-statement.png',
                onclick: function () {
                    
                    if (confirm('All content in this editor will be replaced with VIP Media Statement template, Article Category will be set to Kenyataan Media and Show Title in Options tab will be set to Hide! Are you sure?'))  {
                      	
                      	var html = '<div style="text-align: center;"><img src="images/logo_kpt_black.svg" alt="logo kpt black" width="300" height="115" /></div>';
                  		html += '<h1 style="text-align: center;">KENYATAAN MEDIA</h1>';
                  		html += '<h1 style="text-align: center;">[SILA MASUKKAN TAJUK KENYATAAN MEDIA]</h1>';
                  		html += '<hr />';
                  		html += '<p style="text-align: justify;">[SILA MASUKKAN KANDUNGAN]</p>';
                  		html += '<p><strong>[NAMA YBM/KSU]<br />[JAWATAN YBM/KSU]</strong></p>';
                      	html += '<p><strong>[MASUKKAN TARIKH DI SINI]</strong></p>';
                  		html += '<h2>Muat Turun:</h2>'
                    	html += '<p>[FILE MUAT TURUN DI SINI]</p>'
                      	
                  		// Change Options : Show Title to hide
                      	document.getElementById('jform_attribs_show_title').value = '0';
                      	// Change Article Category to Kenyataan Media
                      	document.getElementById('jform_catid').value = '13';
                      	//Update jQuery Choosen v1.0 selected
                  		jQuery('#jform_attribs_show_title, #jform_catid').trigger("liszt:updated");
                      	
                    	ed.execCommand('mceSetContent', false, html);
                    }
                    
                }
            });
          
          	ed.addButton('faq', {
                title: 'Template for FAQ',
                image: url + '/img/faq.png',
                onclick: function () {
                    
                    if (confirm('All content in this editor will be replaced with FAQ template, Article Category will be set to Soalan Lazim and Show Title in Options tab will be set to Hide! Are you sure?'))  {
                      	
                      	var html = '<div style="text-align: center;"><img src="images/logo_kpt_black.svg" alt="logo kpt black" width="300" height="115" /></div>';
                  		html += '<h1 style="text-align: center;">SOALAN LAZIM</h1>';
                  		html += '<h1 style="text-align: center;">[SILA MASUKKAN TAJUK SOALAN LAZIM]</h1>';
                  		html += '<hr />';
                  		html += '<p style="text-align: justify;">[SILA MASUKKAN KANDUNGAN]</p>';
                  		html += '<h2>Muat Turun:</h2>'
                    	html += '<p>[FILE MUAT TURUN DI SINI]</p>'
                      	
                  		// Change Options : Show Title to hide
                      	document.getElementById('jform_attribs_show_title').value = '0';
                      	// Change Article Category to Soalan Lazim
                      	document.getElementById('jform_catid').value = '17';
                      	//Update jQuery Choosen v1.0 selected
                  		jQuery('#jform_attribs_show_title, #jform_catid').trigger("liszt:updated");
                      	
                    	ed.execCommand('mceSetContent', false, html);
                    }
                    
                }
            });
          
          	ed.addButton('speeches', {
                title: 'Template for Speeches',
                image: url + '/img/speeches.png',
                onclick: function () {
                    
                    if (confirm('All content in this editor will be replaced with Speeches template, Article Category will be set to Text Ucapan and Show Title in Options tab will be set to Hide! Are you sure?'))  {
                      	
                      	var html = '<div style="text-align: center;"><img src="images/logo_kpt_black.svg" alt="logo kpt black" width="300" height="115" /></div>';
                  		html += '<h1 style="text-align: center;">TEKS UCAPAN</h1>';
                  		html += '<h3 style="text-align: center;">[NAMA VIP]<br />[JAWATAN VIP]</h3>';
                      	html += '<h2 style="text-align: center;">[TAJUK MAJLIS]</h2>';
                      	html += '<h4 style="text-align: center;">[TARIKH]</h4>';
                  		html += '<hr />';
                  		html += '<p style="text-align: justify;">[SILA MASUKKAN KANDUNGAN]</p>';
                  		html += '<h2>Muat Turun:</h2>'
                    	html += '<p>[FILE MUAT TURUN DI SINI]</p>'
                      	
                  		// Change Options : Show Title to hide
                      	document.getElementById('jform_attribs_show_title').value = '0';
                      	// Change Article Category to Soalan Lazim
                      	document.getElementById('jform_catid').value = '18';
                      	//Update jQuery Choosen v1.0 selected
                  		jQuery('#jform_attribs_show_title, #jform_catid').trigger("liszt:updated");
                      	
                    	ed.execCommand('mceSetContent', false, html);
                    }
                    
                }
            });

			ed.addButton('br2p', {
                title: 'Change break inside Paragraph to multiple Paragraphs',
                image: url + '/img/code.png',
                onclick: function () {
                  	
                  	var SelectedContent = ed.selection.getContent();
                  	var CleanContent;
                  	if (SelectedContent == null || SelectedContent == "") {
                      	alert('Please select text!');
                    } else {
                      	CleanContent = SelectedContent.replaceAll('<br />','</p><p>');
                      	ed.execCommand('mceInsertRawHTML', false, CleanContent);
                    }
                    
                }
            });
          	
          	ed.addButton('clean-content', {
                title: 'Clean content from unwanted/repeated code',
                image: url + '/img/brush.png',
                onclick: function () {
                  	var CleanContent
                    
                    // 1st run to clean and pass to editor for reformatting, 2nd to reclean
                    for (let i=0; i < 2; i++) {
                   		CleanContent = ed.getContent();
                    	CleanContent = CleanContent.replaceAll('&nbsp;',' ');						// remove non breaking space
                    	CleanContent = CleanContent.replaceAll('</strong><strong>',' ');			// combine repeated formatting
                    	CleanContent = CleanContent.replaceAll('<strong> ',' <strong>');			// better formatting
                    	CleanContent = CleanContent.replaceAll(' </strong>','</strong> ');			// better formatting
                    	CleanContent = CleanContent.replaceAll('</em><em>',' ');					// combine repeated formatting
                    	CleanContent = CleanContent.replaceAll('<em> ',' <em>');					// better formatting
                    	CleanContent = CleanContent.replaceAll(' </em>','</em> ');					// better formatting
                    	CleanContent = CleanContent.replaceAll(/\W+(data-mce-word).*?\d\"/ig,' ');	// delete Microsoft Word data
                        
                        // delete all empty content (p, h1, h2, span etc)
                        CleanContent = CleanContent.replaceAll(/(((<\w+>)+[ \n(<br>)]*(<\/\w+>)+)+)/ig,'');
                        
                        // Send to editor for reformatting
                        ed.execCommand('mceSetContent', false, CleanContent);
                    }
                    
                    // get content from editor
                    CleanContent = ed.getContent();
                    
                    // delete anchor with empty innerHTML
                    CleanContent = CleanContent.replaceAll(/<a\s+(?:[^>]*?\s+).*?><\/a>/ig,' ');
                    
                    // Send to editor for reformatting
                    ed.execCommand('mceSetContent', false, CleanContent);
                  	
                }
            });
          
        }
    });
    tinymce.PluginManager.add('borhan', tinymce.plugins.CustomHTML);
})();