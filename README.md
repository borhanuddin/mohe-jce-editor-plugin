# MoHE JCE Editor Plugin - For MoHE Official Portal content template

## Description
Custom Plugin for JCE Editor Spesifically design to be use only for MoHE Official Portal 2021.
- Add Image gallery code for Simple Image Gallery (by JoomlaWorks) version 4.1.0 
- Template for MoHE Media Statement
- Template for VIP Media Statement
- Change selected break inside paragraph to multiple paragraphs
- Clean content from unwanted code (&nbsp;, microsoft word specific code and etc)

## Requirement
- JCE Editor
- Simple Image Gallery (by JoomlaWorks) version 4.1.0 (modified version by Borhanuddin)